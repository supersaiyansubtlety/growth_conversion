Growth Conversion is for the [Fabric mod loader](https://www.fabricmc.net/) and Minecraft 1.16.

Download it on [CurseForge](https://www.curseforge.com/minecraft/mc-mods/growth-conversion)!

This mod is in *beta*, expect a few bugs, and there may be changes to datapack format. 

Growth Conversion adds 'growth recipes' that cause the blocks around a newly grown plant to convert to a different kind of block. 

This was inspired by the mechanic that converts grass to podzol when a large spruce tree grows, and is (approximately) a generalization of that mechanic. 

Growth recipes are added using datapacks and are highly customizable. 

Things you can customize include:
- which block was planted
- which block was grown
- which block was grown upon
- any number of blocks to convert to any number of other blocks
- several parameters that control how far away converted blocks can be

Information about datapack format will be available on the wiki. 

When connecting to a server, clients do not need this mod installed.

[![Requires the Fabric API](https://i.imgur.com/Ol1Tcf8.png)](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files/all?filter-game-version=1738749986%3a70886)

This mod is only for Fabric and I won't be porting it to Forge. The license is [MIT](https://will-lucic.mit-license.org/), however, so anyone else is free to port it. 

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required. 
