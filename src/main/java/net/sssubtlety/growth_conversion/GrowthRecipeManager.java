package net.sssubtlety.growth_conversion;

import com.google.gson.*;
import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;
import net.fabricmc.fabric.api.resource.ResourceReloadListenerKeys;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.loot.LootTable;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tag.ServerTagManagerHolder;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.profiler.Profiler;
import net.minecraft.util.registry.Registry;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static net.sssubtlety.growth_conversion.GrowthConversionInit.MOD_IDENTIFIER;
import static net.sssubtlety.growth_conversion.GrowthConversionUtil.singletonLootTable;
import static net.sssubtlety.growth_conversion.GrowthRecipe.clearRecipes;

public class GrowthRecipeManager extends JsonDataLoader implements IdentifiableResourceReloadListener {
    private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
    private static final String TAG_KEY = "tag";
    private static final String BLOCK_KEY = "block";
    private static final String PLANTED_BLOCK_KEY = "planted_block";
    private static final String GROWN_BLOCK_KEY = "grown_block";
    private static final String CONVERSION_MAP_KEY = "conversion_map";

    private static final String MIN_RADIUS_KEY = "min_radius";
    private static final String MAX_RADIUS_KEY = "max_radius";
    private static final String MIN_RELATIVE_Y_KEY = "min_relative_Y";
    private static final String MAX_RELATIVE_Y_KEY = "max_relative_Y";
    private static final String REQUIRE_CONTIGUITY_KEY = "require_contiguity";

    private static final String PLANTED_ON_BLOCK_KEY = "planted_on_block";

    private static Map<Identifier, JsonElement> loader;

    public GrowthRecipeManager(String dataType) {
        super(GSON, dataType);
    }

    /*
     `demo_recipe.json`
     {
       "planted_block": "brown_mushroom", // optional, "#" prefix means id is of a tag
       "grown_block": "mushroom_stem", // required, "#" also allowed here
       "planted_on_block": "minecraft:mycelium", // optional, "#" also allowed here
        "conversion_map": { // required, must be object
          "#leaves": "mycelium" //must have at lest one entry
        },
        "min_radius": 5, // optional, int
        "max_radius": 7, // optional, int
        "min_relative_Y": -1, // optional, int
        "max_relative_Y": 0, // optional, int
        "require_contiguity": true // optional, boolean
     }
    */

    @Override
    protected void apply(Map<Identifier, JsonElement> loader, ResourceManager manager, Profiler profiler) {
        GrowthRecipeManager.loader = loader;
        MinecraftServer server = GrowthConversionInit.getServer();
        if (server != null) {
            finishApply(server);
        }
    }

    public static void finishApply(MinecraftServer server) {
        clearRecipes();
        loader.forEach((id, jsonElement) -> {
            if (!jsonElement.isJsonObject())
                throw new JsonSyntaxException("Growth recipe " + id + " is not an object. ");

            JsonObject json = jsonElement.getAsJsonObject();
            GrowthRecipe.Builder recipeBuilder = new GrowthRecipe.Builder(id);
            // parse planted_block
            JsonElement plantedBlockElement = json.get(PLANTED_BLOCK_KEY);
            if (plantedBlockElement != null)
                recipeBuilder.plantedBlock(getBlockTag(id, plantedBlockElement, PLANTED_BLOCK_KEY));

            // parse grown_block
            JsonElement grownBlockElement = json.get(GROWN_BLOCK_KEY);
            if (grownBlockElement == null)
                throw new JsonSyntaxException("Growth recipe " + id + " has no " + GROWN_BLOCK_KEY + " specified. ");

            recipeBuilder.grownBlock(getBlockTag(id, grownBlockElement, GROWN_BLOCK_KEY));
            
            // parse planted_on_block
            JsonElement plantedOnElement = json.get(PLANTED_ON_BLOCK_KEY);
            if (plantedOnElement != null)
                recipeBuilder.plantedOnBlock(getBlockTag(id, plantedOnElement, PLANTED_ON_BLOCK_KEY));

            // parse conversion_map
            JsonObject mapObject = JsonHelper.getObject(json, CONVERSION_MAP_KEY);
            for (Map.Entry<String, JsonElement> entry : mapObject.entrySet()) {
                Tag<Block> fromBlockTag = getBlockTag(id, new JsonPrimitive(entry.getKey()),
                        CONVERSION_MAP_KEY + " entry key");

                final JsonElement element = entry.getValue();
                if (element.isJsonPrimitive()) {
                    // format: "(#)namespace:path"
                    String idString = JsonHelper.asString(element, CONVERSION_MAP_KEY + " entry value");
                    if (idString.startsWith("#")) {
                        // loot table
                        putTable(recipeBuilder, fromBlockTag, idString.substring(1), id, server);
                    } else {
                        // item
                        putBlock(recipeBuilder, fromBlockTag, idString, id);
                    }
                } else {
                    // object
                    String type;
                    String idString;
                    JsonObject object = JsonHelper.asObject(element, CONVERSION_MAP_KEY + " entry value");
                    if (object.size() == 1) {
                        // format: { "loottable/block": "namespace:path" }
                        Map.Entry<String, JsonElement> onlyEntry = object.entrySet().stream().iterator().next();
                        type = onlyEntry.getKey();
                        idString = JsonHelper.asString(onlyEntry.getValue(), CONVERSION_MAP_KEY + " entry value");
                    } else {
                        // format:
                        // {
                        //   "type": "loottable/block",
                        //    "id": "namespace:path"
                        // {
                        type = JsonHelper.getString(object, "type");
                        idString = JsonHelper.getString(object, "id");
                    }

                    switch (type) {
                        case "loottable":
                            putTable(recipeBuilder, fromBlockTag, idString, id, server);
                        case "block":
                            putBlock(recipeBuilder, fromBlockTag, idString, id);
                        default:
                            throw new JsonSyntaxException("Found unrecognized type in entry value in " + CONVERSION_MAP_KEY + " in growth_recipe " + id);
                    }
                }
            }

            if (json.has(MIN_RADIUS_KEY))
                recipeBuilder.minRadius(JsonHelper.getInt(json, MIN_RADIUS_KEY));
            if (json.has(MAX_RADIUS_KEY))
                recipeBuilder.maxRadius(JsonHelper.getInt(json, MAX_RADIUS_KEY));
            if (json.has(MIN_RELATIVE_Y_KEY))
                recipeBuilder.minRelativeY(JsonHelper.getInt(json, MIN_RELATIVE_Y_KEY));
            if (json.has(MAX_RELATIVE_Y_KEY))
                recipeBuilder.maxRelativeY(JsonHelper.getInt(json, MAX_RELATIVE_Y_KEY));
            if (json.has(REQUIRE_CONTIGUITY_KEY))
                recipeBuilder.requireContiguity(JsonHelper.getBoolean(json, REQUIRE_CONTIGUITY_KEY));

            try {
                recipeBuilder.put();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        });
        GrowthRecipeManager.loader = null;
    }

    private static void putBlock(GrowthRecipe.Builder recipeBuilder, Tag<Block> fromBlockTag, String idString, Identifier id) {
        Identifier blockId = new Identifier(idString);
        Block block = Registry.BLOCK.get(blockId);
        if (block != Registry.BLOCK.get(-1))
            recipeBuilder.putConversion(fromBlockTag, singletonLootTable(block));
        else
            throw new JsonParseException("Block " + blockId + " does not exist. Referenced in growth_recipe " + id);
    }

    private static void putTable(GrowthRecipe.Builder recipeBuilder, Tag<Block> fromBlockTag, String idString, Identifier id, MinecraftServer server) {
        final Identifier tableId = new Identifier(idString);
        LootTable table = server.getLootManager().getTable(tableId);
        if (table != LootTable.EMPTY)
            recipeBuilder.putConversion(fromBlockTag, table);
        else
            throw new JsonParseException("Loot table " + tableId + " does not exist. Referenced in growth_recipe " + id);
    }

    private static Tag<Block> getBlockTag(Identifier id, JsonElement element, final String elementName) {
        if (element.isJsonPrimitive()) {
            JsonPrimitive planted_blockPrimitive = element.getAsJsonPrimitive();
            if (!planted_blockPrimitive.isString())
                throw wrongInputJsonType(id);

            String blockOrTagString = planted_blockPrimitive.getAsString();
            if (blockOrTagString.startsWith("#"))
                return getBlockTagFromTagString(id, blockOrTagString.substring(1), elementName);
            else
                return new SingletonTag<>(getBlockFromBlockString(id, blockOrTagString, elementName));

        } else {
            // element must be object
            if (!element.isJsonObject())
                throw wrongInputJsonType(id);

            JsonObject blockOrTagObject = element.getAsJsonObject();
            if (blockOrTagObject.has(TAG_KEY))
                return getBlockTagFromTagString(id, JsonHelper.getString(blockOrTagObject, TAG_KEY), elementName);
            else if (blockOrTagObject.has(BLOCK_KEY))
                return new SingletonTag<>(getBlockFromBlockString(id, JsonHelper.getString(blockOrTagObject, BLOCK_KEY), elementName));
            else
                throw new JsonSyntaxException(elementName + " for growth recipe " + id + " contains neither a " + TAG_KEY + " nor a " + BLOCK_KEY + ". ");

        }
    }

    private static Tag<Block> getBlockTagFromTagString(Identifier id, String planted_blockString, String elementName) {
        Tag<Block> planted_blockTag = ServerTagManagerHolder.getTagManager().getBlocks().getTag(new Identifier(planted_blockString));
        if (planted_blockTag == null)
            throw new JsonSyntaxException(elementName + " block tag " + planted_blockString + " in " + "growth recipe " + id + " does not exist. ");

        return planted_blockTag;
    }

    private static Block getBlockFromBlockString(Identifier id, String blockString, String elementName) {
        Block planted_blockBlock  = Registry.BLOCK.get(new Identifier(blockString));
        if (planted_blockBlock == Blocks.AIR)
            throw new JsonSyntaxException(elementName + " block " + blockString + " in " + "growth recipe " + id + " does not exist. ");
        return planted_blockBlock;
    }

    private static JsonSyntaxException wrongInputJsonType(Identifier id) {
        return new JsonSyntaxException("Growth recipe " + id + "'s planted_block is nether a string nor an object. ");
    };

    @Override
    public Identifier getFabricId() {
        return MOD_IDENTIFIER;
    }

    @Override
    public Collection<Identifier> getFabricDependencies() {
        return Collections.singleton(ResourceReloadListenerKeys.TAGS);
    }
}
