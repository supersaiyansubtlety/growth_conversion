package net.sssubtlety.growth_conversion;

import net.minecraft.item.ItemStack;
import net.minecraft.loot.condition.LootCondition;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.function.ConditionalLootFunction;
import net.minecraft.loot.function.LootFunctionType;

public class LimitToBlocksLootFunction extends ConditionalLootFunction {
    protected LimitToBlocksLootFunction(LootCondition[] conditions) {
        super(conditions);
    }

    @Override
    protected ItemStack process(ItemStack stack, LootContext context) {
        return null;
    }

    @Override
    public LootFunctionType getType() {
        return null;
    }
}
