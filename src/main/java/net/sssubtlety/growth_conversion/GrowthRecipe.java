package net.sssubtlety.growth_conversion;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.AutomaticItemPlacementContext;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextType;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.tag.Tag;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.sssubtlety.growth_conversion.mixin.BlockItemAccessor;

import java.util.*;

import static net.sssubtlety.growth_conversion.GrowthConversionInit.LOGGER;
import static net.sssubtlety.growth_conversion.GrowthConversionUtil.singletonLootTable;

public class GrowthRecipe {
    public static final GrowthRecipeManager MANAGER = new GrowthRecipeManager("growth_recipes");
    protected static final Map<Block, Pair<GrowthRecipe, Tag<Block>>> PLANTED_RECIPE_MAP = new HashMap<>();
    protected static final Map<Block, GrowthRecipe> GROWN_RECIPE_MAP = new HashMap<>();
    public static final Direction[] DIRECTIONS = Direction.values();
    public static final BlockState AIR = Blocks.AIR.getDefaultState();

    protected final Identifier id;
//    protected final Tag<Block> plantedBlock;
//    protected final Tag<Block> grownBlock;
    protected final Tag<Block> plantedOnBlock;
    protected final Map<Block, LootTable> conversionMap;
    protected final int minRadius;
    protected final int maxRadius;
    protected final int radiusRange;
    protected final int minRelativeY;
    protected final int maxRelativeY;
    protected final boolean requireContiguity;

    protected GrowthRecipe(Identifier id, /*Tag<Block> plantedBlock, Tag<Block> grownBlock,*/ Tag<Block> plantedOnBlock, Map<Block, LootTable> conversionMap, int minRadius, int maxRadius, int minRelativeY, int maxRelativeY, boolean requireContiguity) {
        this.id = id;
//        this.plantedBlock = plantedBlock;
//        this.grownBlock = grownBlock;
        this.plantedOnBlock = plantedOnBlock;
        this.conversionMap = conversionMap;
        this.minRadius = minRadius;
        this.maxRadius = maxRadius;
        this.radiusRange = maxRadius - minRadius + 1;
        this.maxRelativeY = maxRelativeY;
        this.minRelativeY = minRelativeY;
        this.requireContiguity = requireContiguity;
    }

//    public static void addRecipe(GrowthRecipe.Builder builder) throws InstantiationException {
//        builder.verify();
//
//        if (builder.plantedOnBlock != null) {
//
//        } else {
//            List<Block> grownBlocks = builder.grownBlock.values();
//            for (Block block : grownBlocks)
//                GROWN_RECIPE_MAP.put(block, recipe);
//        }
//    }

    public static void clearRecipes() {
        GROWN_RECIPE_MAP.clear();
    }

    public static GrowthRecipe getGrowthRecipe(Block block) {
        return GROWN_RECIPE_MAP.get(block);
    }

    public static void tryGrowthConversion(ServerWorld world, Random random, BlockPos pos, BlockState plantedState) {
        GrowthRecipe recipe;
        if (plantedState == null) {
            recipe = GROWN_RECIPE_MAP.get(world.getBlockState(pos).getBlock());
            if (recipe == null) return;
        } else {
            Block plantedBlock = plantedState.getBlock();
            Pair<GrowthRecipe, Tag<Block>> recipePair = PLANTED_RECIPE_MAP.get(plantedBlock);
            if (recipePair == null) return;
            recipe = recipePair.getLeft();
            if (recipe == null) return;
            if (!recipePair.getRight().contains(world.getBlockState(pos).getBlock())) return;
        }

        recipe.doConversion(world, random, pos);
    }

    public void doConversion(ServerWorld world, Random random, BlockPos pos) {
        Vec3d vecPos = new Vec3d(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5);
        final Set<BlockPos> seen = new HashSet<>();
        final Set<BlockPos> converted = new HashSet<>();

        // sin(stepAngle/2) = (stepSize/2)/radius; stepSize = 1;
        // asin(1/(2*radius)) = stepAngle/2;
        // 2*asin(1/(2*radius)) = stepAngle;
        // 2*asin(1/2*1/radius) = stepAngle;

        // stepSize = radius * stepAngle; stepSize = 1
        // 1/radius = stepAngle
        final double stepAngle = 2 * GrowthConversionUtil.asin(0.5f / maxRadius);
        // 2pi / steps = stepAngle
        // 2pi / stepAngle = steps
        final int steps = MathHelper.ceil(GrowthConversionUtil.TWO_PI / stepAngle);

        for (int yOffset = minRelativeY; yOffset <= maxRelativeY; yOffset++) {
            // try to convert center block and mark it converted to ignore it for contiguity
            final Vec3d center = vecPos.add(0, yOffset, 0);
            final BlockPos blockPos = new BlockPos(center);
            tryConvertBlockAtPos(blockPos, seen, converted, world, random);
            converted.add(blockPos);

            for (float step = 0; step < steps; step++) {
                int curMaxRadius = (radiusRange <= 0 ? 0 : random.nextInt(radiusRange)) + minRadius;
                final float radAngle = (float)(step * stepAngle);
                double yStep = MathHelper.sin(radAngle);
                double xStep = MathHelper.cos(radAngle);
                for (int radius = 1; radius <= curMaxRadius; radius++) {
                    // must calculate exact pos and round to get BlockPos
                    if (!tryConvertBlockAtPos(new BlockPos(center.add(radius * xStep, 0, radius * yStep)),
                            seen, converted, world, random) && requireContiguity)
                        break;
                }

                for (; curMaxRadius <= maxRadius; curMaxRadius++)
                    seen.add(new BlockPos(center.add(curMaxRadius * xStep, 0, curMaxRadius * yStep)));
            }
        }
    }

    private boolean tryConvertBlockAtPos(BlockPos pos, Set<BlockPos> seen, Set<BlockPos> converted, ServerWorld world, Random random) {
        if (seen.contains(pos))
            return converted.contains(pos);
        seen.add(pos);
        BlockState stateAtPos = world.getBlockState(pos);
        LootTable mappedTable = conversionMap.get(stateAtPos.getBlock());// getMappedLootTable(stateAtPos.getBlock(), world.getServer());
        if (mappedTable == null)// LootTable.EMPTY)
            return false;

        LootContext.Builder ctxBuilder = (new LootContext.Builder(world)).random(random);
        final ItemStack outputStack = mappedTable.generateLoot(ctxBuilder.build((new LootContextType.Builder()).build())).get(0);
        Item outputItem = outputStack.getItem();
        if (outputItem instanceof BlockItem) {
            world.setBlockState(pos, AIR);
            if (!tryPlace(pos, world, outputStack, (BlockItem) outputItem))
                // restore the original block if the output couldn't be placed
                world.setBlockState(pos, stateAtPos);
            else {
                converted.add(pos);
                return true;
            }

        } else
            LOGGER.warn("Loot table " + mappedTable + " of " + this.id +  "produced non-block item: " + outputItem);

        return false;
    }

    private boolean tryPlace(BlockPos pos, ServerWorld world, ItemStack outputStack, BlockItem blockItem) {
        // try to place facing each direction and against each side
        for (Direction facing : DIRECTIONS) {
            for (Direction side : DIRECTIONS) {
                final AutomaticItemPlacementContext automaticContext = new AutomaticItemPlacementContext(world, pos, facing, outputStack, side);
                final BlockState placementState = blockItem.getBlock().getPlacementState(automaticContext);
                if (((BlockItemAccessor)blockItem).callCanPlace(automaticContext, placementState)) {
                    world.setBlockState(pos, placementState);
                    return true;
                }
//                if (ActionResult.FAIL != blockItem.place(new AutomaticItemPlacementContext(world, pos, facing, outputStack, side)))
//                    return true;
            }
        }
        return false;
    }

    public static class Builder {
        protected final Identifier id;
        protected Tag<Block> plantedBlock;
        protected Tag<Block> grownBlock;
        protected Tag<Block> plantedOnBlock;
        protected Map<Block, LootTable> conversionMap;
        protected int minRadius;
        protected int maxRadius;
        protected int minRelativeY;
        protected int maxRelativeY;
        protected boolean requireContiguity;

        public Builder(Identifier id) {
            this.id = id;
            this.plantedBlock = null;
            this.grownBlock = null;
            this.plantedOnBlock = null;
            this.conversionMap = null;
            this.minRadius = 2;
            this.maxRadius = 10;
            this.minRelativeY = -1;
            this.maxRelativeY = 0;
            this.requireContiguity = true;
        }

        protected GrowthRecipe build() throws InstantiationException {
            verify();
            return new GrowthRecipe(id, plantedBlock, /*grownBlock, plantedOnBlock,*/ conversionMap, minRadius, maxRadius, minRelativeY, maxRelativeY, requireContiguity);
        }

        public void put() throws InstantiationException {
            this.verify();

            if (this.plantedBlock != null) {
                for (Block planted : this.plantedBlock.values())
                    PLANTED_RECIPE_MAP.put(planted, new Pair<>(this.build(), this.grownBlock));
            } else {
                for (Block grown : this.grownBlock.values())
                    GROWN_RECIPE_MAP.put(grown, this.build());
            }
        }

        private void verify() throws InstantiationException {
            if (grownBlock == null)
                throw new InstantiationException("grown has not been initialized. ");
            if (conversionMap == null)
                throw new InstantiationException("conversionMap has not been initialized. ");

            if (conversionMap.isEmpty())
                throw new InstantiationException("conversionMap cannot be empty. ");

            if (minRadius < 0)
                throw new InstantiationException("minRadius cannot be negative. ");
            if (maxRadius < minRadius)
                throw new InstantiationException("maxRadius cannot be less than minRadius. ");

            if (maxRelativeY < minRelativeY)
                throw new InstantiationException("maxRelativeY cannot be less than minRelativeY. ");
        }

        public Builder plantedBlock(Tag<Block> plantedBlock) {
            this.plantedBlock = plantedBlock;
            return this;
        }

        public Builder plantedBlock(Block plantedBlock) {
            this.plantedBlock = new SingletonTag<>(plantedBlock);
            return this;
        }

        public Builder grownBlock(Tag<Block> grownBlock) {
            this.grownBlock = grownBlock;
            return this;
        }

        public Builder grownBlock(Block grownBlock) {
            this.grownBlock = new SingletonTag<>(grownBlock);
            return this;
        }

        public Builder plantedOnBlock(Tag<Block> plantedOnBlock) {
            this.plantedOnBlock = plantedOnBlock;
            return this;
        }

        public Builder plantedOnBlock(Block plantedOnBlock) {
            this.plantedOnBlock = new SingletonTag<>(plantedOnBlock);
            return this;
        }

        public Builder conversionMap(Map<Block, LootTable> conversionMap) {
            this.conversionMap = conversionMap;
            return this;
        }

        public Builder conversionTagMap(Map<Tag<Block>, LootTable> conversionTagMap) {
            this.conversionMap = new HashMap<>();
            // expand the tags
            conversionTagMap.forEach(this::flattenAndPutConversion);
            return this;
        }

        protected void flattenAndPutConversion(Tag<Block> tag, LootTable toTable) {
            for (Block block : tag.values())
                this.conversionMap.put(block, toTable);
        }

        public Builder putConversion(Tag<Block> fromBlockTag, LootTable toTable) {
            if (conversionMap == null)
                conversionMap = new HashMap<>();

            flattenAndPutConversion(fromBlockTag, toTable);
            return this;
        }

        public Builder putConversion(Tag<Block> fromBlockTag, Block toBlock) {
            if (conversionMap == null)
                conversionMap = new HashMap<>();

            flattenAndPutConversion(fromBlockTag, singletonLootTable(toBlock));
            return this;
        }


        public Builder minRadius(int minRadius) {
            this.minRadius = minRadius;
            return this;
        }

        public Builder maxRadius(int maxRadius) {
            this.maxRadius = maxRadius;
            return this;
        }

        public Builder minRelativeY(int minRelativeY) {
            this.minRelativeY = minRelativeY;
            return this;
        }

        public Builder maxRelativeY(int maxRelativeY) {
            this.maxRelativeY = maxRelativeY;
            return this;
        }

        public Builder requireContiguity(boolean requireContiguity) {
            this.requireContiguity = requireContiguity;
            return this;
        }
    }
}
