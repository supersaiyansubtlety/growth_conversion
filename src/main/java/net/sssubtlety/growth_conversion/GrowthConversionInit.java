package net.sssubtlety.growth_conversion;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.minecraft.resource.ResourceType;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.Identifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GrowthConversionInit implements ModInitializer {
	public static final String MOD_ID = "growth_conversion";
	public static final Identifier MOD_IDENTIFIER = new Identifier(MOD_ID, MOD_ID);
	public static final Logger LOGGER = LogManager.getLogger();

	private static MinecraftServer server = null;

	@Override
	public void onInitialize() {
		ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(GrowthRecipe.MANAGER);

		ServerLifecycleEvents.SERVER_STARTING.register(server -> {
			GrowthConversionInit.server = server;
			GrowthRecipeManager.finishApply(server);
		});

		ServerLifecycleEvents.SERVER_STOPPING.register(server -> GrowthConversionInit.server = null);

		// hard-coded testing recipes
//		ServerWorldEvents.LOAD.register((MinecraftServer server, ServerWorld serverWorld) -> {
//			GrowthRecipe.Builder recipeBuilder = new GrowthRecipe.Builder(new Identifier(MOD_ID, "test_recipe"));
//			LootTable.Builder lootBuilder = LootTable.builder().pool(LootPool.builder().rolls(ConstantLootTableRange.create(1)).with(ItemEntry.builder(Blocks.NETHERITE_BLOCK)));
//			Map<Block, LootTable> conversionMap = new HashMap<>();
//			conversionMap.put(Blocks.GRASS_BLOCK, lootBuilder.build());
//			recipeBuilder.conversionMap(conversionMap);
//			recipeBuilder.requireContiguity(true);
//
//			try {
//				recipeBuilder.plantedBlock(Blocks.JUNGLE_SAPLING);
//				recipeBuilder.minRadius(9);
//				recipeBuilder.maxRadius(10);
//				addGrowthRecipe(recipeBuilder.build());
//
//				recipeBuilder.plantedBlock(Blocks.OAK_SAPLING);
//				recipeBuilder.minRadius(8);
//				recipeBuilder.maxRadius(10);
//				addGrowthRecipe(recipeBuilder.build());
//
//				recipeBuilder.plantedBlock(Blocks.BIRCH_SAPLING);
//				recipeBuilder.minRadius(7);
//				recipeBuilder.maxRadius(10);
//				addGrowthRecipe(recipeBuilder.build());
//
//			} catch (InstantiationException e) {
//				e.printStackTrace();
//			}
//		});
	}

	public static MinecraftServer getServer() {
		return server;
	}
}
