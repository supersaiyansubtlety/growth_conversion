package net.sssubtlety.growth_conversion;

import com.google.common.collect.Lists;
import net.minecraft.tag.Tag;

import java.util.Collections;
import java.util.List;

public class SingletonTag<T> implements Tag<T> {
    protected final T entry;
    protected final List<T> singletonList;

    public SingletonTag(T entry) {
        this.entry = entry;
        this.singletonList = Collections.singletonList(entry);
    }

    @Override
    public boolean contains(T entry) {
        return entry.equals(this.entry);
    }

    @Override
    public List<T> values() {
        return singletonList;
    }
}
