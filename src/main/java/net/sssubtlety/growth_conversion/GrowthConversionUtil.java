package net.sssubtlety.growth_conversion;

import net.minecraft.item.ItemConvertible;
import net.minecraft.loot.ConstantLootTableRange;
import net.minecraft.loot.LootPool;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.entry.ItemEntry;
import net.sssubtlety.growth_conversion.mixin.MathHelperAccessor;

public interface GrowthConversionUtil {
    double TWO_PI = (2 * Math.PI);

    static LootTable singletonLootTable(ItemConvertible item) {
        return LootTable.builder().pool(LootPool.builder().rolls(ConstantLootTableRange.create(1))
                .with(ItemEntry.builder(item))).build();
    }

    static float asin(float f) {
        return (float) MathHelperAccessor.getARCSINE_TABLE()[(int)(f * 256 / TWO_PI) & 0xff];
    }
}
