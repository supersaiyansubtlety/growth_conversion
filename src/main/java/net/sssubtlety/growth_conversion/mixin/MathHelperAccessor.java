package net.sssubtlety.growth_conversion.mixin;

import net.minecraft.util.math.MathHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(MathHelper.class)
public interface MathHelperAccessor {
    @Accessor
    static double[] getARCSINE_TABLE() {
        throw new UnsupportedOperationException();
    }
}
