package net.sssubtlety.growth_conversion.mixin;

import net.minecraft.block.BlockState;
import net.minecraft.item.BoneMealItem;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.sssubtlety.growth_conversion.GrowthRecipe;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(BoneMealItem.class)
public abstract class BoneMealItemMixin {
    private static BlockState growState;
    private static BlockState growSeagrassState;
    private static BlockPos growSeagrassPos;

    @Inject(method = "useOnFertilizable", locals = LocalCapture.CAPTURE_FAILEXCEPTION,
            at = @At(value = "INVOKE", target = "Lnet/minecraft/block/Fertilizable;grow(Lnet/minecraft/server/world/ServerWorld;Ljava/util/Random;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;)V"))
    private static void preGrow(ItemStack arg0, World world, BlockPos pos, CallbackInfoReturnable<Boolean> cir, BlockState state) {
        growState = state;
//        GrowthRecipe.tryPreGrowthConversion((ServerWorld) world, world.random, pos, state);
    }

    @Inject(method = "useOnFertilizable", at = @At(value = "INVOKE", shift = At.Shift.AFTER,
                    target = "Lnet/minecraft/block/Fertilizable;grow(Lnet/minecraft/server/world/ServerWorld;Ljava/util/Random;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;)V"))
    private static void postGrow(ItemStack arg0, World world, BlockPos pos, CallbackInfoReturnable<Boolean> cir) {
        GrowthRecipe.tryGrowthConversion((ServerWorld) world, world.random, pos, growState);
    }

    @Inject(method = "useOnGround", locals = LocalCapture.CAPTURE_FAILEXCEPTION,
            at = @At(value = "INVOKE", target = "Lnet/minecraft/block/Fertilizable;grow(Lnet/minecraft/server/world/ServerWorld;Ljava/util/Random;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;)V"))
    private static void preGrowSeagrass(ItemStack stack, World world, BlockPos wrongPos, Direction facing, CallbackInfoReturnable<Boolean> cir, int unused1, BlockPos pos, BlockState unused3, BlockState state) {
        growSeagrassState = state;
        growSeagrassPos = pos;
//        GrowthRecipe.tryPreGrowthConversion((ServerWorld) world, world.random, pos, state);
    }

    @Inject(method = "useOnGround", at = @At(value = "INVOKE", shift = At.Shift.AFTER,
                    target = "Lnet/minecraft/block/Fertilizable;grow(Lnet/minecraft/server/world/ServerWorld;Ljava/util/Random;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;)V"))
    private static void postGrowSeagrass(ItemStack stack, World world, BlockPos wrongPos, Direction facing, CallbackInfoReturnable<Boolean> cir) {
        GrowthRecipe.tryGrowthConversion((ServerWorld) world, world.random, growSeagrassPos, growSeagrassState);
    }
}
