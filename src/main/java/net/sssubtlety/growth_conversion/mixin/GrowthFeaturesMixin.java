package net.sssubtlety.growth_conversion.mixin;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.*;
import net.sssubtlety.growth_conversion.GrowthRecipe;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Random;

@Mixin({
        TreeFeature.class,
        ChorusPlantFeature.class,  // doesn't actually have an effect outside of world generation
//        HugeRedMushroomFeature.class,  // not direct descendant
//        HugeBrownMushroomFeature.class,  // not direct descendant
        HugeMushroomFeature.class,
        HugeFungusFeature.class
})
public abstract class GrowthFeaturesMixin {
//    private static final ImmutableSet<Feature<?>> GROWTH_FEATURES = ImmutableSet.of(
//            Feature.TREE,
//            Feature.CHORUS_PLANT,
//            Feature.HUGE_RED_MUSHROOM,
//            Feature.HUGE_BROWN_MUSHROOM,
//            Feature.HUGE_FUNGUS
//    );

//    private BlockState preState;
//    @Inject(method = "generate(Lnet/minecraft/world/StructureWorldAccess;Lnet/minecraft/world/gen/chunk/ChunkGenerator;Ljava/util/Random;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/world/gen/feature/FeatureConfig;)Z", at = @At("HEAD"))
//    void preGenerate(StructureWorldAccess world, ChunkGenerator chunkGenerator, Random random, BlockPos pos, FeatureConfig featureConfig, CallbackInfoReturnable<Boolean> cir) {
////        if (cir.getReturnValue()){// && GROWTH_FEATURES.contains(this)) {
//            preState = world.getBlockState(pos);
////        }
//    }
    @Inject(method = "generate(Lnet/minecraft/world/StructureWorldAccess;Lnet/minecraft/world/gen/chunk/ChunkGenerator;Ljava/util/Random;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/world/gen/feature/FeatureConfig;)Z", at = @At("TAIL"))
    void postGenerate(StructureWorldAccess world, ChunkGenerator chunkGenerator, Random random, BlockPos pos, FeatureConfig featureConfig, CallbackInfoReturnable<Boolean> cir) {
        if (cir.getReturnValue()) {// && GROWTH_FEATURES.contains(this)) {
            GrowthRecipe.tryGrowthConversion(world.toServerWorld(), random, pos, null);
        }
    }
}
