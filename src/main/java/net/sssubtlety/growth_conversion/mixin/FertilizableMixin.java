package net.sssubtlety.growth_conversion.mixin;

import net.minecraft.block.*;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Random;

// AbstractPlantBlock, AbstractPlantStemBlock: kelp, twisting, weeping
@Mixin({
        AbstractPlantBlock.class, AbstractPlantStemBlock.class, BambooBlock.class,
        CropBlock.class, FernBlock.class, FungusBlock.class, FungusBlock.class,
        GrassBlock.class, MushroomPlantBlock.class, NetherrackBlock.class,
        NyliumBlock.class, SaplingBlock.class, SeaPickleBlock.class,
        SeagrassBlock.class, StemBlock.class, SweetBerryBushBlock.class,
        TallFlowerBlock.class
})
public abstract class FertilizableMixin {
    private static final Logger LOGGER = LogManager.getLogger();
    @Inject(method = "grow", at = @At("HEAD"))
    void preGrow(ServerWorld world, Random random, BlockPos pos, BlockState state, CallbackInfo ci) {
        LOGGER.info("Grew!");
    }
}
