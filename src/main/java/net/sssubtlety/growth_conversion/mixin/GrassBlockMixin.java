package net.sssubtlety.growth_conversion.mixin;

import net.minecraft.block.BlockState;
import net.minecraft.block.GrassBlock;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.sssubtlety.growth_conversion.GrowthRecipe;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Random;

@Mixin(GrassBlock.class)
public abstract class GrassBlockMixin {
    private static BlockState preState;

    @Inject(method = "grow",
            at = @At(value = "INVOKE", target = "Lnet/minecraft/block/Fertilizable;grow(Lnet/minecraft/server/world/ServerWorld;Ljava/util/Random;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;)V"))
    private void preGrow(ServerWorld world, Random random, BlockPos pos, BlockState state, CallbackInfo ci) {
        preState = state;
//        GrowthRecipe.tryPreGrowthConversion(world, world.random, pos, state);
    }

    @Inject(method = "grow",
            at = @At(value = "INVOKE", shift = At.Shift.AFTER, target = "Lnet/minecraft/block/Fertilizable;grow(Lnet/minecraft/server/world/ServerWorld;Ljava/util/Random;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;)V"))
    private void postGrow(ServerWorld world, Random random, BlockPos pos, BlockState state, CallbackInfo ci) {
        GrowthRecipe.tryGrowthConversion(world, world.random, pos, preState);
    }
}
